#include "bot.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


double myx;
double myy;

int dist(const void *a, const void *b)
{
    double ax = (*(struct food *)a).x;
    double ay = (*(struct food *)a).y;
    double bx = (*(struct food *)b).x;
    double by = (*(struct food *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}


struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)


{
    struct action act;
    
    myx = me.x;
    myy = me.y;
    
    qsort(foods, nfood, sizeof(struct food), dist);
    
    //act.dx = -10;
   // act.dy = 40;
    //act.fire = 0;
    //act.split = 0;
    act.dx = foods[0].x - me.x;
   // if (foods[0].x==(int)NULL){
      // act.dx = rand() % -rand() % 100; 
      
    //}
    act.dy = foods[0].y - me.y;
    if((foods[0].x==(int)NULL)&&(foods[0].y==(int)NULL)){
        //act.dy = rand() % 100-rand() % 100;
        float point_x, point_y;
        point_x=act.dx;
        point_y=act.dy;
        int ctr;
        for (ctr = 0; ctr < 360; ctr += 1)
        {
            point_x = 5 * cos(ctr * 3.1415926f / 180.0f) + 100;
            point_y = 5 * cos(ctr * 3.1415926f / 180.0f) + 100;
        }
    }
    act.fire = 0;
    act.split = 0;
    
    return act;
}